package com.nalinthip.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec1 = new Rectangle(5, 3);
        System.out.println(rec1.toString());
        System.out.println(rec1.calArea());
        System.out.println(rec1.calPerimeter());
        System.out.println();
        

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());
        System.out.println();
        

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s  area : %.3f \n",circle1.getName(),circle1.calArea());
        System.out.printf("%s  perimeter : %.3f \n",circle1.getName(),circle1.calPerimeter());
        System.out.println();
       

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s  area : %.3f \n",circle2.getName(),circle2.calArea());
        System.out.printf("%s  perimeter : %.3f \n",circle2.getName(),circle2.calPerimeter());
        System.out.println();

        
        Triangle triangle = new Triangle(2, 2, 2);
        System.out.println(triangle);
        System.out.printf("%s  area : %.3f \n",triangle.getName(),triangle.calArea());
        System.out.printf("%s  perimeter : %.3f \n",triangle.getName(),triangle.calPerimeter());
    }
}
